#include <stdio.h>

void print_pointer(int count, char message[], char **name, int *age);

void print_pointer(int count, char message[], char **name, int *age)
{
	for (int i = 0; i < count; i++) {
		printf(message, *(name + i), *(age + i));
	}
}

void print_pointer_as_array(int count, char message[], char **name, int *age)
{
	for (int i = 0; i < count; i++) {
		printf(message, name[i], age[i]);
	}
}

void print_pointer_in_a_stupid_way(int count, char message[], char **name, int *age)
{
	char **cur_name = name;
	int *cur_age = age;

	while((cur_age - age) < count) {
		printf(message, *cur_name, *cur_age);
		cur_name++;
		cur_age++;
	}
}

int main(int argc, const char *argv[])
{
	int ages[] = { 23, 43, 12, 80, 2 };
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"
	};

	// safely get the size of ages
	int count = sizeof(ages) / sizeof(int);
	int i = 0;

	for (i = 0; i < count; i++) {
		printf("%s has been %d years alive.\n", names[i], ages[i]);
	}

	printf("---\n");
	int *cur_age = ages;
	char **cur_name = names;
	char *my_name = names[1];
	
	printf("---\n");
	printf("My name is: %s\n", my_name);
	printf("---\n");

	// second way of using pointers
	/* for (int i = 0; i < count; i++) { */
	print_pointer(count, "%s is %d years old.\n", cur_name, cur_age);
		/* printf("%s is %d years old.\n", */
				/* *(cur_name + i), *(cur_age + i)); */
	/* } */

	printf("---\n");

	// third way, pointers are just arrays
	/* for (int i = 0; i < count; i++) { */
	print_pointer_as_array(count, "%s is %d years old again.\n", cur_name, cur_age);
		/* printf("%s is %d years old again.\n", */
				/* cur_name[i], cur_age[i]); */
	/* } */

	printf("---\n");

	// forth way to use pointers, in a stupidly complicated way

	/* for( cur_name = names, cur_age = ages; */
			/* (cur_age - ages) < count; cur_name++, cur_age++) { */
		/* printf("%s lived %d years so far.\n", */
				/* *cur_name, *cur_age); */
		/* printf("cur_age is %p, ages is %p\n", &cur_age, &ages); */
		/* printf("cur_name is %p, names is %p\n", &cur_name, &names); */
	/* } */

	print_pointer_in_a_stupid_way(count, "%s lived %d years so far.\n", names, ages);
	
	return 0;
}
