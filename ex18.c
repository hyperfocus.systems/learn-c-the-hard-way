#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

// Our old from friend ex17
void die(const char *message)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n", message);
	}

	exit(1);
};

// a typedef creates a fake type
// in this case a function pointer
typedef int (*compare_cb) (int a, int b);
typedef int *(*sort_cb) (int *numbers, int count, compare_cb cmp);

/**
 * A classic bubblesort function that uses the
 * compare_cb to do the sorting
 */
int *bubble_sort(int *numbers, int count, compare_cb cmp)
{
	int temp = 0;
	int i = 0;
	int j = 0;
	int *target = malloc(sizeof(int) * count);

	if (!target)
		die("Merror error in bubble sort");

	memcpy(target, numbers, sizeof(int) * count);

	for (i = 0; i < count; i++) {
		for (j = 0; j < count - 1; j++) {
			if(cmp(target[j], target[j + 1]) > 0) {
				temp = target[j + 1];
				target[j + 1] = target[j];
				target[j] = temp;
			}
		}
	}

	return target;
}

/* Function to sort an array using insertion sort*/
int *insertion_sort(int *numbers, int count, compare_cb cmp) 
{ 
    int i, key, j; 

    int *target = malloc(sizeof(int) * count);

    if (!target)
	    die("Merror error in bubble sort");

    memcpy(target, numbers, sizeof(int) * count);


    for (i = 1; i < count; i++) { 
        key = target[i]; 
        j = i - 1; 
  
        /* Move elements of target[0..i-1], that are 
          greater than key, to one position ahead 
          of their current position */
        while (j >= 0 && cmp(target[j], key) > 0) { 
            target[j + 1] = target[j]; 
            j = j - 1; 
        } 
        target[j + 1] = key; 
    }

    return target;
}

int sorted_order(int a, int b)
{
	return (a > b) - (a < b);
}


int reverse_order(int a, int b)
{
	return (a < b) - (a > b);
}

int strange_order(int a, int b)
{
	if(a == 0 || b == 0) {
		return 0;
	} else {
		return a % b;
	}
}

/**
 * Used to test we are sorting things correctly
 * by doing the sort and printing things out
 * */
void test_sorting(int *numbers, int count, sort_cb sort, compare_cb cmp)
{
	int *sorted = sort(numbers, count, cmp);

	if(!sorted)
		die("Failed to sort as requested");

	for (int i = 0; i < count; i++) {
		printf("%d ", sorted[i]);
	}
	printf("\n");

	free(sorted);

	unsigned char *data = (unsigned char *)cmp;

	for (int i = 0; i < 25; i++) {
		printf("%02x:", data[i]);
	}

	printf("\n");
}

int main(int argc, const char *argv[])
{
	if(argc < 2) die("USAGE: ex18 4 3 1 5 6");

	int count = argc - 1;
	char **inputs = argv + 1;

	int *numbers = malloc(sizeof(int) * count);
	if(!numbers) die("Memory Error at main");

	for (int i = 0; i < count; i++) {
		numbers[i] = atoi(inputs[i]);
	}

	/* test_sorting(numbers, count, bubble_sort, sorted_order); */
	/* test_sorting(numbers, count, bubble_sort, reverse_order); */
	/* test_sorting(numbers, count, bubble_sort, strange_order); */

	test_sorting(numbers, count, insertion_sort, sorted_order);
	test_sorting(numbers, count, insertion_sort, reverse_order);
	test_sorting(numbers, count, insertion_sort, strange_order);

	free(numbers);

	return 0;
}
