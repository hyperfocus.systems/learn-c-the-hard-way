#include <stdio.h>

int main(int argc, const char *argv[])
{

	if(argc < 2) {
		printf("ERROR: You need at least one argument.\n");
		// This is how you abort a program
		return 1;
	}

	int i;
	char letter;
	for (int j = 1; j < argc; j++) {
		char *curr_arg = argv[j];
		for (i = 0; letter = curr_arg[i], curr_arg[i] != '\0'; i++) {
			/* char letter = argv[1][i]; */
			if(letter < 90) {
				letter += 32; // Convert uppercase to lowercase
			}

			switch (letter) {
				case 'a':
					printf("%d 'a':%d \n", i, letter);
					break;

				case 'e':
					printf("%d 'e'\n", i);
					break;

				case 'i':
					printf("%d 'i'\n", i);
					break;

				case 'o':
					printf("%d 'o'\n", i);
					break;

				case 'u':
					printf("%d 'u'\n", i);
					break;

				case 'y':
					// why i > 2? is this a bug?
					if (i > 2) {
						// it's only sometimes Y
						printf("%d 'y'\n", i);
						break;
					}

				default:
					printf("%d: Code %d:%c is not a vowel\n", i, letter, letter);

			}

		}

	}
	return 0;
}
