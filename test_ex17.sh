#!/bin/sh
 set -e

if [[ -f "./test.db" ]]; then
	rm test.db
fi

echo "Create Database"
./ex17 test.db c
echo ""

echo "Add Data"
./ex17 test.db s 0 donathon don@donatoh.com 08567459
./ex17 test.db s 1 "Big Roberto" rob_razor@gmail.com 0478459357
./ex17 test.db s 2 "Dig Dog the Dog man" ruffy_puppy@hotmail.com 32479647
./ex17 test.db s 3 "Brandon Bakker" brandon@bdmd.com.au 0408745712
echo ""

echo "List Data"
./ex17 test.db l
echo ""

echo "Search for data"
echo "name brandon"
./ex17 test.db f name Brandon
./ex17 test.db f name brandon
echo "email ruf"
./ex17 test.db f email ruf
echo "email com"
./ex17 test.db f email com
echo ""

echo "Delete ID 1"
./ex17 test.db d 1
./ex17 test.db l
echo ""

echo "Add a new record into ID 1"
./ex17 test.db s 1 dangdog don2@donatoh.com 085674833
echo ""

echo "List again"
./ex17 test.db l
echo ""
