#include <stdio.h>

int main(int argc, const char *argv[])
{
	int i = 25;

	while(1) {
		printf("%d\n", i);
		if(i < 0) {
			i++;
			continue;
		}
		i--;
	}

	// need this to add final newline
	printf("\n");

	return 0;
}
