#include <stdio.h>

int main(int argc, const char *argv[])
{
	int distance = 100;
	float power = 2.345;
	double super_power = 56789.4532;
	char initial = 'L';
	char first_name[] = "Brandon";
	char last_name[] = "Bakker";

	printf("You are %d kilometers away.\n", distance);
	printf("You have %f levels of power.\n", power);
	printf("You have %f awesome super powers.\n", super_power);
	printf("I have an initial %c.\n", initial);
	printf("I have an first name %s.\n", first_name);
	printf("I have an last name %s.\n", last_name);
	printf("My whole name is %s %c. %s.\n", first_name, initial, last_name);

	int bugs = 100;
	double bug_rate = 1.2;

	printf("You have %d bugs at the imaginary rate of %f.\n", bugs, bug_rate);

	unsigned long universe_of_defects = 9999999999999999999UL;
	printf("The entire universe has %ld bugs.\n", universe_of_defects);

	double expected_bugs = bugs * bug_rate;
	printf("You are expected to have %f bugs.\n",  expected_bugs);

	double part_of_universe = expected_bugs / universe_of_defects;
	printf("This is only a %e portion of the universe\n", part_of_universe);


	// this makes no sense, just a demo of something weird
	char nul_byte = 'a';
	int care_percentage = nul_byte;
	printf("Which means you should care %d%%.\n", care_percentage);

	printf("I am breaking everything %c", nul_byte + 10);

	return 0;
}
