#include <stdio.h>
#include <string.h>

int main(int argc, const char *argv[])
{
	int numbers[4] = { 0 };
	char *name = "a";

	// first, print them out raw
	printf("numbers: %d %d %d %d\n",
			numbers[0], numbers[1], numbers[2], numbers[3]);

	printf("name each: %c %c %c %c\n",
			name[0], name[1], name[2], name[3]);

	printf("name: %s\n", name);

	// setup the numbers
	numbers[0] = 10;
	numbers[1] = 20;
	numbers[2] = 30;
	numbers[3] = 40;

	// setup the name
	name[0] = 'B';
	name[1] = 'r';
	name[2] = 'a';
	name[3] = '\0';

	// print them out itialised

	printf("numbers: %d %d %d %d\n",
			numbers[0], numbers[1], numbers[2], numbers[3]);

	printf("name each: %c %c %c %c\n",
			name[0], name[1], name[2], name[3]);

	// print them like a string
	printf("name: %s\n", name);

	// another way to use name
	char *another = "Bra";

	printf("another %s\n", another);
	
	printf("another each: %c %c %c %c\n",
			another[0], another[1], another[2], another[3]);

	/* memcpy(&numbers, &name, sizeof(name)); */

	/* printf("the worst numbers each: %d %d %d %d\n", */
			/* numbers[0], numbers[1], numbers[2], numbers[3]); */

	return 0;
}
