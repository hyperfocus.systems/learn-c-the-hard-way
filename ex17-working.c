#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#define MAX_DATA 50
#define MAX_ROWS 5

struct Address {
	int id; // 4
	int set; // 4
	char *name; // 8
	char *email; // 8
	char *phone_no; // 8
};

struct Database {
	int max_data; // 4
	int max_rows; // 4
	struct Address **rows;
};

struct Connection {
	FILE *file;
	struct Database *db;
};

void Database_close(struct Connection *conn);

void die(const char *message, struct Connection *conn)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n", message);
	}

	if(conn)
		Database_close(conn);

	exit(1);
};

char* strlwr(char *str)
{
	char *p = str;
	while (*p) {
		*p = tolower(*p);
		p++;
	}

	return str;
}

void Address_print(struct Address *addr)
{
	printf("%d %s %s %s\n", addr->id, addr->name, addr->email, addr->phone_no);
}

void Database_load(struct Connection *conn)
{
	fread(&conn->db->max_data, sizeof(conn->db->max_data), 1, conn->file);
	fread(&conn->db->max_rows, sizeof(conn->db->max_rows), 1, conn->file);

	conn->db->rows = malloc(sizeof(struct Address *) * conn->db->max_rows);

	for (int i = 0; i < conn->db->max_rows; i++) {
		conn->db->rows[i] = malloc(sizeof(struct Address));
		struct Address *row = conn->db->rows[i];
		fread(&row->id, sizeof(row->id), 1, conn->file);
		fread(&row->set, sizeof(row->set), 1, conn->file);

		row->name = malloc(sizeof(*row->name) * conn->db->max_data);
		row->email = malloc(sizeof(*row->email) * conn->db->max_data);
		row->phone_no = malloc(sizeof(*row->phone_no) * conn->db->max_data);

		fread(row->name, sizeof(*row->name) * conn->db->max_data, 1, conn->file);
		fread(row->email, sizeof(*row->email) * conn->db->max_data, 1, conn->file);
		fread(row->phone_no, sizeof(*row->phone_no) * conn->db->max_data, 1, conn->file);
	}
}

struct Connection *Database_open(char *filename, char mode)
{
	struct Connection *conn = malloc(sizeof(struct Connection));
	if(!conn)
		die("Memory Error", conn);

	conn->db = malloc(sizeof(struct Database));
	if(!conn->db)
		die("Memory Error", conn);

	if(mode == 'c') {
		conn->file = fopen(filename, "w");
	} else {
		conn->file = fopen(filename, "r+");

		if(conn->file) {
			Database_load(conn);
		}
	}

	if(!conn->file)
		die("Failed to open file", conn);

	return conn;
};

void Database_close(struct Connection *conn)
{
	if(conn) {
		if(conn->file)
			fclose(conn->file);
		if(conn->db)
			free(conn->db);
		free(conn);
	}
}

void Database_write(struct Connection *conn) {
	rewind(conn->file);

	fwrite(&conn->db->max_data, sizeof(conn->db->max_data), 1, conn->file); // 4
	fwrite(&conn->db->max_rows, sizeof(conn->db->max_rows), 1, conn->file); // 4

	for (int i = 0; i < conn->db->max_rows; i++) {
		struct Address *row = conn->db->rows[i];
		fwrite(&row->id, sizeof(row->id), 1, conn->file); // 4
		fwrite(&row->set, sizeof(row->set), 1, conn->file); // 4
		fwrite(row->name, sizeof(char) * conn->db->max_data, 1, conn->file); // 50
		fwrite(row->email, sizeof(char) * conn->db->max_data, 1, conn->file); // 50
		fwrite(row->phone_no, sizeof(char) * conn->db->max_data, 1, conn->file); // 50
	}

	int rc = fflush(conn->file);
	if( rc == -1)
		die("Could not flush database.", conn);
}

void Database_create(struct Connection *conn, int max_rows, int max_data) {
	conn->db->max_rows = max_rows;
	conn->db->max_data = max_data;
	conn->db->rows = (struct Address**)malloc(sizeof(struct Address*) * max_rows);

	int i = 0;
	for (i = 0; i < max_rows; i++) {
		conn->db->rows[i] = (struct Address*)malloc(sizeof(struct Address));
		conn->db->rows[i]->id = i;
		conn->db->rows[i]->set = 0;
		conn->db->rows[i]->name = (char *)malloc(max_data);
		conn->db->rows[i]->name = (char *)memset(conn->db->rows[i]->name, ' ', max_data); 
		conn->db->rows[i]->email = (char *)malloc(max_data);
		conn->db->rows[i]->email = (char *)memset(conn->db->rows[i]->email, ' ', max_data); 
		conn->db->rows[i]->phone_no = (char *)malloc(max_data);
		conn->db->rows[i]->phone_no = (char *)memset(conn->db->rows[i]->phone_no, ' ', max_data); 
	}
}

void Database_set(struct Connection *conn, int id, const char *name, const char *email, const char *phone_no) {
	int max_data = conn->db->max_data;

	struct Address *addr = conn->db->rows[id];
	if(addr->set)
		die("Already set, delete it first", conn);

	addr->set = 1;
	addr->name = malloc(sizeof(char) * max_data);
	addr->email = malloc(sizeof(char) * max_data);
	addr->phone_no = malloc(sizeof(char) * max_data);

	char *res = strncpy(addr->name, name, max_data);
	if (!res) die("Name copy failed", conn);

	res = strncpy(addr->email, email, max_data);
	if (!res) die("Email copy failed", conn);

	res = strncpy(addr->phone_no, phone_no, max_data);
	if (!res) die("Phone copy failed", conn);
}

void Database_get(struct Connection *conn, int id) {
	struct Address *addr = conn->db->rows[id];

	if(addr->set) {
		Address_print(addr);
	} else {
		die("ID is not set", conn);
	}
}

void Database_delete(struct Connection *conn, int id) {
	free(conn->db->rows[id]);
}

void Database_find(struct Connection *conn, char *field, char *term) {
	struct Database *db = conn->db;

	for (int i = 0; i < db->max_rows; i++) {
		struct Address *curr = db->rows[i];
		char *stored_field = "";

		if (curr->set) {
			if(strcmp(field, "email") == 0) {
				stored_field = strlwr(curr->email);
			}

			if(strcmp(field, "name") == 0) {
				stored_field = strlwr(curr->name);
			}

			if(strcmp(field, "phone") == 0) {
				stored_field = strlwr(curr->phone_no);
			}

			char *lower_term = strlwr(term);
			if(strcasecmp(stored_field, lower_term) == 0) {
				Address_print(curr);
			}

			if(strstr(stored_field, lower_term)) {
				Address_print(curr);
			}
		}
	}
}

void Database_list(struct Connection *conn) {
	int i = 0;
	struct Database *db = conn->db;

	for (i = 0; i < db->max_rows; i++) {
		struct Address *curr = db->rows[i];

		if (curr->set) {
			Address_print(curr);
		}
	}
}

int main(int argc, const char *argv[])
{
	if(argc < 3)
		die("USAGE: ex17 <dbfile> <action> [action params]", NULL);

	const char *filename = argv[1];
	char action = argv[2][0];

	int max_rows = MAX_ROWS;
	int max_data = MAX_DATA;
	if(action == 'c' && argc > 3) {
		if(argv[3]) {
			max_rows = atoi(argv[3]);
			if(max_rows < 1)
				die("Max Row size is invalid", NULL);
		}

		if(argv[4]) {
			max_data = atoi(argv[4]);
			if(max_data < 1)
				die("Max Data size is invalid", NULL);
		}
	}

	/* printf("Set max_rows: %d, max_data: %d.\n", max_rows, max_data); */

	struct Connection *conn = Database_open(filename, action);
	int id = 0;

	if (argc > 3) id = atoi(argv[3]);
	if (id >= MAX_ROWS) die("There's not that many records", conn);

	switch(action) {
		case 'c':
			Database_create(conn, max_rows, max_data);
			Database_write(conn);
			break;
		case 'g':
			if( argc != 4)
				die("Need an id to get", conn);

			Database_get(conn, id);
			break;
		case 's':
			if( argc != 7)
				die("Need id, name, email, phone number to set", conn);

			Database_set(conn, id, argv[4], argv[5], argv[6]);
			Database_write(conn);
			break;
		case 'd':
			if( argc != 4)
				die("Need an id to delete", conn);

			Database_delete(conn, id);
			Database_write(conn);
			break;
		case 'f':
			if(argc != 5)
				die("Need field (name, email, phone), search term", conn);

			Database_find(conn, argv[3], argv[4]);
			break;
		case 'l':
			Database_list(conn);
			break;
		default:
			die("Invalid action: c=create, g=get, s=set, d=del, l=list", conn);
			break;
	}

	Database_close(conn);

	return 0;
}
