#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

struct Stack {
	int capacity;
	int currSize;
	char **items;
};

void die(const char *message)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n", message);
	}

	exit(1);
};

struct Stack* createStack(int size) {
	struct Stack *stack = malloc(sizeof(struct Stack));
	stack->items = malloc(sizeof(char *) * size);
	stack->capacity = size;
	stack->currSize = 0;

	return stack;
}

char* pop(struct Stack *stack) {
	if(stack->currSize == 0)
		die("Stack is empty!");

	char *item = stack->items[stack->currSize - 1];
	stack->items[stack->currSize - 1] = NULL;
	stack->currSize--;
	return item;
}

char* top(struct Stack *stack) {
	if(stack->currSize == 0)
		die("Stack is empty!");

	return stack->items[stack->currSize - 1];
}

void push(struct Stack *stack, char *item) {
	if(stack->currSize == stack->capacity)
		die("Stack is full!!!");

	stack->items[stack->currSize] = item;
	stack->currSize++;
}

int main(int argc, const char *argv[])
{
	struct Stack *stack = createStack(8);

	push(stack, "test");
	push(stack, "test2");
	push(stack, "test3");

	char *top_of_stack = top(stack);
	printf("Top of stack is %s, size is %d\n", top_of_stack, stack->currSize);

	pop(stack);
	top_of_stack = top(stack);
	printf("Curr Top of stack is %s, size is %d\n", top_of_stack, stack->currSize);

	pop(stack);
	top_of_stack = top(stack);
	printf("Curr Top of stack is %s, size is %d\n", top_of_stack, stack->currSize);

	pop(stack);
	top_of_stack = top(stack);
	printf("Curr Top of stack is %s, size is %d\n", top_of_stack, stack->currSize);
	
	return 0;
}
